<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function ct_plan_d_action_v0_2_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function ct_plan_d_action_v0_2_node_info() {
  $items = array(
    'plan_action' => array(
      'name' => t('Plan d\'action'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Libellé'),
      'has_body' => '1',
      'body_label' => t('Description du plan'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
